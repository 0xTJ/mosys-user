import math

with open('fix16_tables.h', 'w') as f:
    f.write('#ifndef INCLUDE_FIX16_TABLES_H\n#define INCLUDE_FIX16_TABLES_H\n\n#include "fix16.h"\n\n')

    f.write('static const fix16_unit_64 sin_table[256] = {\n')
    for i in range(256):
        if i < 128:
            angle = i
        else:
            angle = i - 256
        angle = angle / 256 * (2 * math.pi)

        value = math.sin(angle)
        value = value * 64
        value = round(value)

        f.write(f'    {value},\n')
    f.write('};\n\n')

    f.write('static const fix16_rot_256 asin_table[256] = {\n')
    for i in range(256):
        if i < 128:
            value = i
        else:
            value = i - 256
        value = value / 64
        value = min(value, 1)
        value = max(value, -1)

        angle = math.asin(value)
        angle = angle * 256 / (2 * math.pi)
        angle = round(angle)

        f.write(f'    {angle},\n')
    f.write('};\n\n')

    f.write('#endif\n')
