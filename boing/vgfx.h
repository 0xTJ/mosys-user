#ifndef INCLUDE_VGFX_H
#define INCLUDE_VGFX_H

#include "fix16.h"
#include "util.h"
#include <stdint.h>

struct vgfx_point {
    fix16_unit_64 x;
    fix16_unit_64 y;
    fix16_unit_64 z;
};

struct vgfx_line {
    struct vgfx_point a;
    struct vgfx_point b;
};

void vgfx_screen_draw_line(fix16_unit_64 x_0, fix16_unit_64 y_0, fix16_unit_64 x_1, fix16_unit_64 y_1, uint8_t colour);

void vgfx_line_draw(struct vgfx_line *line, uint8_t colour);

static inline void vgfx_matrix_identity(fix16_unit_64 result[4][4]) {
    for (size_t i = 0; i < 4; ++i) {
        for (size_t j = 0; j < 4; ++j) {
            result[i][j] = i == j ? fix16_unit_64_one : 0;
        }
    }
}

static inline void vgfx_matrix_translation(fix16_unit_64 x, fix16_unit_64 y, fix16_unit_64 z, fix16_unit_64 result[4][4]) {
    vgfx_matrix_identity(result);

    result[0][3] = x;
    result[1][3] = y;
    result[2][3] = z;
}

static inline void vgfx_matrix_rotation(fix16_rot_256 x, fix16_rot_256 y, fix16_rot_256 z, fix16_unit_64 result[4][4]) {
    vgfx_matrix_identity(result);

    fix16_unit_64 sx = x ? fix16_sin(x) : 0;
    fix16_unit_64 sy = y ? fix16_sin(y) : 0;
    fix16_unit_64 sz = z ? fix16_sin(z) : 0;
    fix16_unit_64 cx = x ? fix16_cos(x) : fix16_unit_64_one;
    fix16_unit_64 cy = y ? fix16_cos(y) : fix16_unit_64_one;
    fix16_unit_64 cz = z ? fix16_cos(z) : fix16_unit_64_one;

    result[0][0] = fix16_mul(cz, cy);
    result[0][1] = fix16_mul(fix16_mul(cz, sy), sx) - fix16_mul(sz, cx);
    result[0][2] = fix16_mul(fix16_mul(cz, sy), cx) + fix16_mul(sz, sx);
    result[1][0] = fix16_mul(sz, cy);
    result[1][1] = fix16_mul(fix16_mul(sz, sy), sx) + fix16_mul(cz, cx);
    result[1][2] = fix16_mul(fix16_mul(sz, sy), cx) - fix16_mul(cz, sx);
    result[2][0] = -sy;
    result[2][1] = fix16_mul(cy, sx);
    result[2][2] = fix16_mul(cy, cx);
}

static inline void vgfx_matrix_scaling(fix16_unit_64 x, fix16_unit_64 y, fix16_unit_64 z, fix16_unit_64 result[4][4]) {
    vgfx_matrix_identity(result);

    result[0][0] = x;
    result[1][1] = y;
    result[2][2] = z;
}

static inline void vgfx_matrix_perspective(fix16_unit_64 result[4][4]) {
    vgfx_matrix_identity(result);

    result[3][3] = 0;
    result[3][2] = fix16_unit_64_one;
}

static inline void vgfx_homogeneous_divide(fix16_unit_64 input[4][1], fix16_unit_64 result[4][1]) {
    fix16_unit_64 w_c = input[3][0];
    result[0][0] = fix16_div(input[0][0], w_c);
    result[1][0] = fix16_div(input[1][0], w_c);
    result[2][0] = fix16_div(input[2][0], w_c);
    result[3][0] = fix16_unit_64_one;
}

static inline void vgfx_point_to_position(const struct vgfx_point *point, fix16_unit_64 position[4][1]) {
    position[0][0] = point->x;
    position[1][0] = point->y;
    position[2][0] = point->z;
    position[3][0] = fix16_unit_64_one;
}

void vgfx_bitmap_to_spritemap_16(const uint16_t bitmap[16], uint8_t spritemap[4][8]);
void vgfx_bitmap_to_spritemap_32(const uint32_t bitmap[32], uint8_t spritemap[4][4][8]);

void vgfx_map_half_cylinder_16(uint16_t bitmap[16]);
void vgfx_map_circle_16(uint16_t bitmap[16]);
void vgfx_map_half_sphere_16(uint16_t bitmap[16]);

void vgfx_map_half_cylinder_32(uint32_t bitmap[32]);
void vgfx_map_circle_32(uint32_t bitmap[32]);
void vgfx_map_half_sphere_32(uint32_t bitmap[32]);

#endif
