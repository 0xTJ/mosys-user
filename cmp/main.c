#include <errno.h>
#include <err.h>
#include <fcntl.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdalign.h>
#include <sched.h>
#include <stdlib.h>
#include <string.h>
#include <spawn.h>
#include <time.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/wait.h>

unsigned char buf_a[512];
unsigned char buf_b[512];

int main(int argc, char **argv, char **envp) {
    if (argc < 1) {
        errx(EXIT_FAILURE, "No argument vector entries");
    }
    if (argc < 3) {
        errx(EXIT_FAILURE, "Usage: %s a b", argv[0]);
    }

    int a_fd = open(argv[1], O_RDONLY);
    if (a_fd < 0) {
        err(EXIT_FAILURE, "Failed to open(\"%s\")", argv[1]);
    }

    int b_fd = open(argv[2], O_RDONLY);
    if (b_fd < 0) {
        err(EXIT_FAILURE, "Failed to open(\"%s\")", argv[2]);
    }

    size_t read_total = 0;
    while (1) {
        ssize_t read_a_result = read(a_fd, buf_a, sizeof(buf_a));
        ssize_t read_b_result = read(b_fd, buf_b, sizeof(buf_a));

        ssize_t read_result = read_a_result < read_b_result ? read_a_result : read_b_result;

        if (read_result < 0) {
            err(EXIT_FAILURE, "Failed to read");
        } else if (read_result == 0) {
            break;
        }

        for (size_t i = 0; i < read_result; ++i) {
            if (buf_a[i] != buf_b[i]) {
                printf(
                    "Mismatch at byte %zu: A = %02hhX B = %02hhX\n",
                    read_total + i,
                    buf_a[i], buf_b[i]);
            }
        }

        read_total += read_result;
    }
}
