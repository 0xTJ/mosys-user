#include <errno.h>
#include <err.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdalign.h>
#include <sched.h>
#include <stdlib.h>
#include <string.h>
#include <spawn.h>
#include <time.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/wait.h>

unsigned char buf[512];

int main(int argc, char **argv, char **envp) {
    if (argc < 1) {
        errx(EXIT_FAILURE, "No argument vector entries");
    }
    if (argc < 3) {
        errx(EXIT_FAILURE, "Usage: %s source destination", argv[0]);
    }

    int src_fd = open(argv[1], O_RDONLY);
    if (src_fd < 0) {
        err(EXIT_FAILURE, "Failed to open(\"%s\")", argv[1]);
    }

    int dest_fd = open(argv[2], O_WRONLY | O_CREAT);
    if (dest_fd < 0) {
        err(EXIT_FAILURE, "Failed to open(\"%s\")", argv[2]);
    }

    while (1) {
        ssize_t read_result = read(src_fd, buf, sizeof(buf));

        if (read_result < 0) {
            err(EXIT_FAILURE, "Failed to read");
        } else if (read_result == 0) {
            break;
        }

        ssize_t write_result = write(dest_fd, buf, read_result);
    }
}
