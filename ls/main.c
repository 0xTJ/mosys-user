#include <errno.h>
#include <err.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdalign.h>
#include <sched.h>
#include <stdlib.h>
#include <string.h>
#include <spawn.h>
#include <time.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/wait.h>

int main(int argc, char **argv, char **envp) {
    if (argc < 1) {
        errx(EXIT_FAILURE, "No argument vector entries");
    }
    if (argc < 2) {
        errx(EXIT_FAILURE, "Usage: %s dir", argv[0]);
    }

    int dir_fd = open(argv[1], 0);
    if (dir_fd < 0) {
        err(EXIT_FAILURE, "Failed to open(\"%s\")", argv[1]);
    }

    alignas(struct dirent) char buf[0x100];

    while (1) {
        struct dirent *dirp = (void *) &buf;

        ssize_t count_read = getdents(dir_fd, dirp, sizeof(buf));
        if (count_read == -1) {
            err(EXIT_FAILURE, "Failed to getdents()");
        } else if (count_read == 0) {
            break;
        }

        size_t bpos;
        for (bpos = 0; bpos < (size_t) count_read;) {
            printf("%10d %s\n", (int) dirp->d_ino, dirp->d_name);

            size_t inc_size = sizeof(struct dirent);
            dirp = (struct dirent *) ((char *) dirp + inc_size);
            bpos += inc_size;
        }
    }

    return EXIT_SUCCESS;
}
