#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <sched.h>
#include <stdlib.h>
#include <string.h>
#include <spawn.h>
#include <time.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include "util.h"

enum simplefs_entry_type {
    simplefs_entry_type_volume_identifier   = 0x01,
    simplefs_entry_type_starting_marker     = 0x02,
    simplefs_entry_type_unused              = 0x10,
    simplefs_entry_type_directory           = 0x11,
    simplefs_entry_type_file                = 0x12,
    simplefs_entry_type_unusable            = 0x18,
    simplefs_entry_type_deleted_directory   = 0x19,
    simplefs_entry_type_deleted_file        = 0x1A,
};

#pragma pack(push, 1)

struct simplefs_superblock {
    uint8_t reserved_0[11];             // Reserved for compatibility (boot code)
    uint8_t reserved_1[21];             // Reserved for compatibility (legacy BIOS parameter block)
    uint8_t reserved_2[372];            // Reserved for compatibility (boot code)
    uint64_t time_stamp;                // Time stamp
    uint64_t data_size_blocks;          // Size of data area in blocks
    uint64_t index_size_bytes;          // Size of index area in bytes
    union {
        struct {
            union {
                struct {
                    uint8_t magic[3];   // Magic number (0x534653)
                    uint8_t version;    // Simple File System version number (0x10 for Version 1.0)
                };
                uint32_t magic_version; // 0x10534653
            };
            uint64_t total_blocks;      // Total number of blocks
            uint32_t reserved_blocks;   // Number of reserved blocks
            uint8_t block_size;         // Block size
            uint8_t checksum;           // Checksum
        };
        uint8_t checksummed[18];
    };
    uint8_t reserved_3[64];             // Reserved for compatibility (partition table)
    uint8_t reserved_4[2];              // Reserved for compatibility (boot signature)
};
_Static_assert(sizeof(struct simplefs_superblock) == 0x200, "SimpleFS superblock has wrong size");

union simplefs_entry {
    struct {
        uint8_t entry_type;
    };
    struct simplefs_entry_volume_identifier {
        uint8_t entry_type;             // Entry type (0x01 for the Volume Identifier Entry)
        uint8_t reserved_0[3];          // Unused/reserved (must be zero)
        uint64_t time_stamp;            // Time stamp
        uint8_t name[52];               // Volume name in UTF-8, including zero terminator
    } volume_identifier;
    struct simplefs_entry_starting_marker {
        uint8_t entry_type;             // Entry type (0x02 for the Starting Marker Entry)
        uint8_t reserved_0[63];         // Unused/reserved
    } starting_marker;
    struct simplefs_entry_unused {
        uint8_t entry_type;             // Entry type (0x10 for Unused Entries)
        uint8_t reserved_0[63];         // Unused/reserved
    } unused;
    struct simplefs_entry_directory {
        uint8_t entry_type;             // Entry type (0x11 for Directory Entries)
        uint8_t continuation_entries;   // Number of continuation entries following this entry
        uint64_t time_stamp;            // Time stamp
        uint8_t name[54];               // Directory name in UTF-8
    } directory;
    struct simplefs_entry_file {
        uint8_t entry_type;             // Entry type (0x12 for File Entries)
        uint8_t continuation;           // Number of continuation entries following this entry
        uint64_t time_stamp;            // Time stamp
        uint64_t starting_block;        // Starting block number in the data area
        uint64_t ending_block;          // Ending block number in the data area
        uint64_t file_length;           // File length in bytes
        uint8_t name[30];               // File name in UTF-8
    } file;
    struct simplefs_entry_unusable {
        uint8_t entry_type;             // Entry type (0x18 for Unusable Entries)
        uint8_t reserved_0[9];          // Unused/reserved
        uint64_t starting_block;        // Starting block number in the data area
        uint64_t ending_block;          // Ending block number in the data area
        uint8_t reserved_1[38];         // Unused/reserved
    } unusable;
    struct simplefs_entry_deleted_directory {
        uint8_t entry_type;             // Entry type (0x19 for Deleted Directory Entries)
        uint8_t continuation_entries;   // Number of continuation entries following this entry
        uint64_t time_stamp;            // Time stamp
        uint8_t name[54];               // Directory name in UTF-8
    } deleted_directory;
    struct simplefs_entry_deleted_file {
        uint8_t entry_type;             // Entry type (0x1A for Deleted File Entries)
        uint8_t continuation;           // Number of continuation entries following this entry
        uint64_t time_stamp;            // Time stamp
        uint64_t starting_block;        // Starting block number in the data area
        uint64_t ending_block;          // Ending block number in the data area
        uint64_t file_length;           // File length in bytes
        uint8_t name[30];               // File name in UTF-8
    } deleted_file;
    struct simplefs_entry_continuation {
        uint8_t name[64];               // Entry name in UTF-8
    } continuation;
};
_Static_assert(sizeof(union simplefs_entry) == 0x40, "SimpleFS index entry has wrong size");

#pragma pack(pop)

static uint8_t simplefs_checksum_validate(struct simplefs_superblock *sfs_sb) {
    uint8_t sum = 0;
    for (unsigned i = 0; i < sizeof(sfs_sb->checksummed); ++i) {
        sum += sfs_sb->checksummed[i];
    }
    return sum;
}

// Uses 512-byte blocks
void format(int fd, uint32_t reserved_block, uint64_t total_blocks, const char *name) {
    off_t lseek_result;
    ssize_t write_result;

    struct simplefs_superblock sb = {0};
    union simplefs_entry entries[3] = {0};

    unsigned char block_size_width = 2;
    size_t block_size = 1ULL << (block_size_width + 7);

    sb.checksum = 0;
    sb.block_size = to_little_u8(block_size_width);
    sb.reserved_blocks = to_little_u32(reserved_block);
    sb.total_blocks = to_little_u64(total_blocks);
    sb.magic_version = 0x53465310;
    sb.index_size_bytes = to_little_u64(sizeof(entries));
    sb.data_size_blocks = to_little_u64(0);
    sb.time_stamp = to_little_u64(0);
    sb.checksum = 0x100 - simplefs_checksum_validate(&sb);

    lseek_result = lseek(fd, 0, SEEK_SET);
    if (lseek_result < 0) {
        err(EXIT_FAILURE, "Failed to lseek() superblock");
    }
    write_result = write(fd, &sb, sizeof(sb));
    if (write_result < 0) {
        err(EXIT_FAILURE, "Failed to write() superblock");
    }

    entries[0].starting_marker.entry_type = to_little_u8(simplefs_entry_type_starting_marker);

    entries[1].directory.entry_type = to_little_u8(simplefs_entry_type_directory);
    entries[1].directory.continuation_entries = to_little_u8(0);
    entries[1].directory.time_stamp = to_little_u64(0);
    strncpy((char *) entries[1].directory.name, "", sizeof(entries[1].directory.name) - 1);
    entries[1].directory.name[sizeof(entries[1].directory.name) - 1] = '\0';

    entries[2].volume_identifier.entry_type = to_little_u8(simplefs_entry_type_volume_identifier);
    entries[2].volume_identifier.time_stamp = to_little_u64(0);
    strncpy((char *) entries[2].volume_identifier.name, name, sizeof(entries[2].volume_identifier.name) - 1);
    entries[2].volume_identifier.name[sizeof(entries[2].volume_identifier.name) - 2] = '\0';

    printf("Entries to offset %lX\n", (unsigned long) (block_size * total_blocks - sizeof(entries)));
    printf("End entry to offset %lX\n", (unsigned long) (block_size * total_blocks - sizeof(entries) + sizeof(entries[0]) * 2));

    lseek_result = lseek(fd, block_size * total_blocks - sizeof(entries), SEEK_SET);
    if (lseek_result < 0) {
        err(EXIT_FAILURE, "Failed to lseek() entries");
    }
    write_result = write(fd, &entries, sizeof(entries));
    if (write_result < 0) {
        err(EXIT_FAILURE, "Failed to write() entries");
    }
}

int main(int argc, char **argv, char **envp) {
    if (argc < 2) {
        errx(EXIT_FAILURE, "Usage: %s device [name]", argv[0]);
    }

    const char *path = argv[1];
    const char *name = argc > 2 ? argv[2] : "";

    int dev_at25 = open(path, O_RDWR);
    if (dev_at25 < 0) {
        err(EXIT_FAILURE, "Failed to open(%s)", path);
    }
    format(dev_at25, 1, 1024, name);
    close(dev_at25);
    dev_at25 = -1;

    ssize_t result;

    dev_at25 = open(path, O_RDWR);
    if (dev_at25 < 0) {
        err(EXIT_FAILURE, "Failed to open(%s)", path);
    }

    close(dev_at25);
    dev_at25 = -1;

    return EXIT_SUCCESS;
}
