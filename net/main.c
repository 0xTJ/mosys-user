#include <errno.h>
#include <err.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <sched.h>
#include <time.h>
#include <unistd.h>
#include <spawn.h>
#include <stdlib.h>
#include <string.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <sys/un.h>

const char *sock_name = "x";

int main(int argc, char **argv, char **envp) {
    int sv[2];
    if (socketpair(AF_UNIX, SOCK_STREAM, 0, sv) != 0) {
        err(EXIT_FAILURE, "Failed to socketpair()");
    }

    if (send(sv[0], "lol", 3, 0) < 0) {
        err(EXIT_FAILURE, "Failed to send()");
    }

    char buf[10] = {0};
    if (recv(sv[1], buf, 3, 0) < 0) {
        err(EXIT_FAILURE, "Failed to recv()");
    }
    printf("GOT %s\n", buf);

    close(sv[0]);

    // char buf2[10] = {0};
    // if (recv(sv[1], buf2, 3, 0) < 0) {
    //     err(EXIT_FAILURE, "Failed to recv()");
    // }
    // printf("GOT %s\n", buf2);


    // struct sockaddr_un addr;
    // addr.sun_family = AF_UNIX;
    // addr.sun_path[0] = '\0';
    // size_t sock_name_len = strlen(sock_name);
    // strcpy(&addr.sun_path[1], sock_name);
    // socklen_t addrlen = offsetof(struct sockaddr_un, sun_path) + 1 + sock_name_len;

    // if (argc == 1) {
    //     int sock_recv = socket(AF_UNIX, SOCK_STREAM, 0);
    //     if (sock_recv < 0) {
    //         err(EXIT_FAILURE, "Failed to socket() recv");
    //     }

    //     if (bind(sock_recv, (struct sockaddr *) &addr, addrlen) < 0) {
    //         err(EXIT_FAILURE, "Failed to bind()");
    //     }

    //     char *sh_argv[] = { "/bin/sh", NULL };
    //     posix_spawn(NULL, sh_argv[0], NULL, NULL, sh_argv, envp);

    //     struct sockaddr_un accept_addr;
    //     socklen_t accept_addrlen = sizeof(accept_addr);
    //     int sock_accept = accept(sock_recv, (struct sockaddr *) &accept_addr, &accept_addrlen);

    //     if (sock_accept < 0) {
    //         err(EXIT_FAILURE, "Failed to accept()");
    //     }
    // } else {
    //     int sock_send = socket(AF_UNIX, SOCK_STREAM, 0);
    //     if (sock_send < 0) {
    //         err(EXIT_FAILURE, "Failed to socket() send");
    //     }

    //     if (connect(sock_send, (struct sockaddr *) &addr, addrlen) < 0) {
    //         err(EXIT_FAILURE, "Failed to connect()");
    //     }
    // }

    return EXIT_SUCCESS;
}
